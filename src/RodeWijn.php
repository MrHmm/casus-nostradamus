<?php

Namespace App;

class RodeWijn extends Product
{
    public $naam;

    public $kwaliteit;

    public $verkopenVoor;

    public function tick()
    {
        $this->verkopenVoor--;
        $this->kwaliteit = $this->plusOneBeneathFifty($this->kwaliteit);
        if ($this->verkopenVoor < 0) {
            $this->kwaliteit = $this->plusOneBeneathFifty($this->kwaliteit);
        }
    }
}
