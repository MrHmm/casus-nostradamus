<?php


namespace App;

class Product
{
    public $naam;

    public $kwaliteit;

    public $verkopenVoor;

    public function __construct($naam, $kwaliteit, $verkopenVoor)
    {
        $this->naam         = $naam;
        $this->kwaliteit    = $kwaliteit;
        $this->verkopenVoor = $verkopenVoor;
    }

    public static function of($naam, $kwaliteit, $verkopenVoor) {
        return new static($naam, $kwaliteit, $verkopenVoor);
    }

    public function tick() {

    }

    public function minusOneStayPositive(int $int, int $amountOfTimes = 1) {
        if ($int > 0) {
            --$int;
        }
        if(--$amountOfTimes > 0) {
            $int = $this->minusOneStayPositive($int, --$amountOfTimes);
        }
        return $int;
    }

    public function plusOneBeneathFifty(int $int) {
        if ($int < 50) {
            ++$int;
        }
        return $int;
    }
}
