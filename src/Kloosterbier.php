<?php

Namespace App;

class Kloosterbier extends Product
{
    public $naam;

    public $kwaliteit;

    public $verkopenVoor;

    public function tick()
    {
        $this->verkopenVoor--;
        $this->kwaliteit = $this->minusOneStayPositive($this->kwaliteit, 2);
        if ($this->verkopenVoor < 0) {
            $this->kwaliteit = $this->minusOneStayPositive($this->kwaliteit, 2);
        }
    }
}
