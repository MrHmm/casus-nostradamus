<?php

Namespace App;

class WitteWijn extends Product
{
    public $naam;

    public $kwaliteit;

    public $verkopenVoor;

    public function tick()
    {
        $this->verkopenVoor--;
        if($this->verkopenVoor >= 0) {
            $this->kwaliteit = $this->plusOneBeneathFifty($this->kwaliteit);
            if ($this->verkopenVoor < 10) {
                $this->kwaliteit = $this->plusOneBeneathFifty($this->kwaliteit);
            }       
            if ($this->verkopenVoor < 5) {
                $this->kwaliteit = $this->plusOneBeneathFifty($this->kwaliteit);
            }
        } else {
            $this->kwaliteit = 0;
        }
    }
}
