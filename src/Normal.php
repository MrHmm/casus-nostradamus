<?php

Namespace App;

class Normal extends Product
{
    public $naam;

    public $kwaliteit;

    public $verkopenVoor;
    public function tick()
    {
        $this->verkopenVoor--;
        $this->kwaliteit = $this->minusOneStayPositive($this->kwaliteit);
        if ($this->verkopenVoor < 0) {
            $this->kwaliteit = $this->minusOneStayPositive($this->kwaliteit);
        }
    }
}
